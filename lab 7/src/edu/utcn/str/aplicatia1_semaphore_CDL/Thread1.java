package edu.utcn.str.aplicatia1_semaphore_CDL;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class Thread1 extends Thread {
    CountDownLatch count;
    Semaphore P9, P10;

    Thread1(Semaphore P9, Semaphore P10, CountDownLatch count) {
        this.count = count;
        this.P9 = P9;
        this.P10 = P10;
    }

    public void run() {
        while (true) {
            Locatie P1 = new Locatie();
            P1.activity(2, 4, 1);
            Tranzitie T2 = new Tranzitie();
            try {
                P9.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            T2.activity(2);
            Locatie P4 = new Locatie();
            try {
                P10.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            P4.activity(4, 5, 4);
            Tranzitie T4 = new Tranzitie();
            T4.activity(4);
            Locatie P6 = new Locatie();
            P6.activity(6);
            Tranzitie T6 = new Tranzitie();
            T6.activity(4, 6);
            P9.release();
            P10.release();
            Locatie P7 = new Locatie();
            P7.activity(7);

            count.countDown();

            try {
                System.out.println(count+" t1");
                count.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("S-a terminat thread 1");
        }
    }

}

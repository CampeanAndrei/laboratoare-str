package edu.utcn.str.aplicatia3;

import java.util.concurrent.CountDownLatch;

public class MainAp3 {

    public static void main(String[] args) {
        int x=5;
        int y=9;


        Object P6 = 1;
        Object P10 = 2;
		CountDownLatch countDownLatch = new CountDownLatch(3);

        Fir1 fir1 = new Fir1(P6, P10,countDownLatch, 2, 3, 7);
        Fir2 fir2 = new Fir2(P6,countDownLatch, 3, 5,x);
        Fir3 fir3 = new Fir3(P10,countDownLatch, 4, 6,y);
        fir1.start();
        fir2.start();
        fir3.start();

        try {
            fir1.join();
            fir2.join();
            fir3.join();
        } catch (InterruptedException ignored) {


        }

        Tranzitie T11 = new Tranzitie();
        T11.activity(11);
    }
}

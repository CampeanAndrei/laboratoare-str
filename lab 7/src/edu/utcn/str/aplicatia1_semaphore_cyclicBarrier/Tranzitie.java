package edu.utcn.str.aplicatia1_semaphore_cyclicBarrier;

public class Tranzitie
{


    public void activity(int intT, int NrTranzitie)
    {
        System.out.println("Tranzitia T" +NrTranzitie +" timpul tranzitiei:"+intT);
        try
        {
            Thread.currentThread().sleep(intT*1000);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void activity(int NrTranzitie)
    {

        System.out.println("Tranzitia T"+NrTranzitie);
    }
}

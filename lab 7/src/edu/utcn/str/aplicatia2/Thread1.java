package edu.utcn.str.aplicatia2;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

public class Thread1 extends Thread {
    Lock P9;
    CountDownLatch count;

    Thread1(Lock P9, CountDownLatch count) {
        this.count = count;
        this.P9 = P9;
    }

    public void run() {
    while (true) {
        Locatie P1 = new Locatie();
        P1.activity(1);
        Tranzitie T2 = new Tranzitie();
        P9.lock();
        T2.Tran(2);
        Locatie P4 = new Locatie();
        P4.activity(2, 4, 4);
        Tranzitie T4 = new Tranzitie();
        T4.Tran(4, 4);
        P9.unlock();
        Locatie P6 = new Locatie();
        P6.activity(6);

          count.countDown();
        try {
            System.out.println(count + " t2");
            count.await();

        } catch (Exception e) {
            System.out.println("Eroare count down latch");
        }
    }


    }
}

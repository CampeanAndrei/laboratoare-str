package edu.utcn.str.aplicatia2;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

public class Thread2 extends Thread {
    Lock P9;
    Lock P10;
    CountDownLatch count;

    Thread2(Lock P9, Lock P10, CountDownLatch count) {
        this.count = count;
        this.P10 = P10;
        this.P9 = P9;
    }

    public void run() {
        while (true) {
            Locatie P11 = new Locatie();
            P11.activity(11);
            Tranzitie T11 = new Tranzitie();
            P9.lock();
            P10.lock();
            T11.Tran(11);
            Locatie P12 = new Locatie();
            P12.activity(3, 6, 12);
            Tranzitie T12 = new Tranzitie();
            T12.Tran(3, 12);
            P9.unlock();
            P10.unlock();

            Locatie P13 = new Locatie();
            P13.activity(13);

                count.countDown();
            try {
                System.out.println(count + " t2");
                count.await();

            } catch (Exception e) {
                System.out.println("Eroare count down latch");
            }
        }

    }
}
